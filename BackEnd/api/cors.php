<?php
header('Content-Type: text/html; charset=utf-8');
header('Access-Control-Allow-Origin: *');
header('Access-Control-Allow-Methods: POST');
header('Access-Control-Allow-Headers: Content-Type');
// ini_set('display_errors', 1);
// echo '<!-- Hello, world! -->';
// error_reporting(-1);
date_default_timezone_set('Asia/Calcutta');
setlocale(LC_MONETARY, 'en_IN');
if(isset($_REQUEST['q'])){
  $url = filter_var($_REQUEST['q'], FILTER_SANITIZE_URL);
  $response = file_get_contents($url);
  echo $response;
}
else{
  echo "invalid request";
}
?>
