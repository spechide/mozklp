
var BACKEND_URL = "//mozklp-1310.appspot.com";

var RequestData = function(type, URL, formData, callBack){
  // create a XHR object
  var xhr = new XMLHttpRequest();
  // open the XHR object in asynchronous mode
  xhr.open(type, URL, true);
  if(type == "POST"){
    xhr.setRequestHeader('Content-type', 'application/x-www-form-urlencoded; charset=ISO-8859-1');
  }
  xhr.onreadystatechange = function() {
    if (xhr.readyState == 4 && xhr.status == 200) {
      // OK! we have a successful response.
      var response = xhr.responseText;
      //console.log('OUTPUT: ' + response);
      // do something else with the response
      callBack(URL, response);
    }
  };
  // GET or POST the URL according to type
  if(type == "GET"){
    xhr.send();
  }
  if(type == "POST"){
    xhr.send(formData);
  }
};

var FormatNumber = function(n){
  // => http://stackoverflow.com/a/8043056/4723940
  return n > 9 ? "" + n: "0" + n;
};

var GetCurrentDate = function(callback){
  // => http://stackoverflow.com/a/3552493/4723940
  /*
    var date = new Date();
    var day = date.getDate();
    var monthIndex = date.getMonth();
    var year = date.getFullYear();
    var requiredValues = year + "-" + FormatNumber(monthIndex + 1) + "-" + FormatNumber(day);
    return requiredValues;
  */
  // <= according to me, if the client's system date is wrong Then the above
  //    function would not work! So,
  RequestData("POST", BACKEND_URL + "/CurrentDate", "", function(u, r){
    callback(r);
  });
};

var ParseUTCString = function(UTCString){
  var year = UTCString.substring(0, 4);
  var month = UTCString.substring(5, 7);
  var day = UTCString.substring(8, 10);
  return {
    year, month, day
  };
};

// => http://stackoverflow.com/a/1643468/4723940
var MonthCodes = ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sept", "Oct", "Nov", "Dec"];
var MonthNames = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"];

var getlocFromLatLng = function(lat, lng, callback){
  RequestData("POST", 'https://maps.googleapis.com/maps/api/geocode/json?latlng=' + lat + ',' + lng + '&sensor=true', "", function(u, r){
    var locationName = JSON.parse(r).results;
    callback(lat, lng, locationName[0].formatted_address);
  });
};

var GetNextEvents = function(API_URL, current_date){
  RequestData("POST", BACKEND_URL + "/CORS/", "q=" + encodeURIComponent(API_URL + "?offset=0&limit=0&start__gte=" + current_date + "&query=kerala"), function(u, r){
    // console.log(r);
    var jsonobj = JSON.parse(r);
    var total_pending_events = jsonobj.meta.total_count;
    if(total_pending_events > 0){
      var i = 0;
      // <= temporarily just display the immediately next event
      var firstEvent = jsonobj.objects[i];
      var start = firstEvent.start;
      var dateevents = ParseUTCString(start);
      document.getElementById('eventapi_one').innerHTML = '<time datetime="' + start + '"><span class="date-month"><abbr>' + MonthCodes[(dateevents.month - 1)] + '</abbr></span><span class="date-day">' + dateevents.day + '</span></time>';
      var name = firstEvent.name;
      var event_url = firstEvent.event_url;
      document.getElementById('eventapi_two').href = event_url;
      document.getElementById('eventapi_two').innerHTML = "<h4>" + name + "</h4>";
      document.getElementById('eventapi_three').datetime = start;
      document.getElementById('eventapi_three').innerHTML = MonthNames[(dateevents.month - 1)] + " " + dateevents.day + ", " + dateevents.year;
      var lat = firstEvent.lat;
      var lng = firstEvent.lon;
      getlocFromLatLng(lat, lng, function(lat, lng, fa){
        document.getElementById('eventapi_four').innerHTML = "<p>" + fa + "</p>";
      });
    }
    else{
      document.getElementById('eventapi_one').innerHTML = '<time datetime="' + "0000-00-00T00:00:00" + '"><span class="date-month"><abbr>' + "No" + '</abbr></span><span class="date-day">' + "00" + '</span></time>';
      document.getElementById('eventapi_two').href = "javascript:void(0);";
      document.getElementById('eventapi_two').innerHTML = "<h4>" + "No New Event" + "</h4>";
      document.getElementById('eventapi_three').datetime = "0000-00-00T00:00:00";
      document.getElementById('eventapi_three').innerHTML = "No" + " " + "00" + ", " + "0000";
      document.getElementById('eventapi_four').innerHTML = "<p>" + "No Location Given" + "</p>";
    }
    // => temporary hack
    $('#status').fadeOut(); // will first fade out the loading animation
    $('#preloader').delay(350).fadeOut('slow'); // will fade out the white DIV that covers the website.
    $('body').delay(350).css({'overflow':'visible'});
    // <= temporary hack
  });
};

$(window).load(function() { // makes sure the whole site is loaded
  GetCurrentDate(function(r){
    GetNextEvents("https://reps.mozilla.org/api/v1/event/", r);
  });
})
